﻿#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>

using namespace std::chrono_literals;


int main()
{
    sf::RenderWindow window(sf::VideoMode(800, 600), "Lab9");
    
    sf::CircleShape shape(100.f);
    shape.setOrigin(100, 100);
    shape.setFillColor(sf::Color::Green);
    int shape_y = 100;
    shape.setPosition(100, shape_y);

    
    sf::RectangleShape shape2(sf::Vector2f(100, 200));
    shape2.setFillColor(sf::Color::Red);
    shape2.setOrigin(50, 100);
    int shape2_y = 100;
    shape2.setPosition(300, shape2_y);

    sf::CircleShape triangle(80.f, 3);
    triangle.setFillColor(sf::Color::Blue);
    int triangle_y = 0;
    triangle.setPosition(400, triangle_y);


    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        shape_y++;
        if (shape_y > 500)
            shape_y = 500;
        shape.setPosition(100, shape_y);

        shape2_y++;
        if (shape2_y > 500)
            shape2_y = 500;
        shape2.setPosition(300, shape2_y);

        triangle_y++;
        if (triangle_y > 479)
            triangle_y = 479;
        triangle.setPosition(400, triangle_y);


        window.clear();
        window.draw(shape);
        window.draw(shape2);
        window.draw(triangle);
        window.display();

       std::this_thread::sleep_for(40ms);
    }

    return 0;
}